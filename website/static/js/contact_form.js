
(function() {
	// admin/integration
     	emailjs.init('user_luubVVB5qNnbK4kTbAjqx');
})();

function mail_submit (event)
{
	event.preventDefault();
   	// generate a five digit number for the contact_number variable
       	this.contact_number.value = Math.random() * 100000 | 0;

	// 'default_service' as configured in user panel
     	emailjs.sendForm('default_service', 'template_gpm2drp', this)
        		.then(function() {
                       		console.log('SUCCESS!');

				document.querySelector("#send-bt").className = "hero-btn-dark-ok"
				document.removeEventListener('submit', mail_submit)

                  	}, function(error) {
                        	console.log('FAILED...', error);
               		});
}




window.onload = function() {

	// reference to the form id
	document.getElementById('contact_form').addEventListener('submit', mail_submit)

}
